const router = require('express').Router({ mergeParams: true });

const user = require('./user')

router
  .get('/', (req, res) => {
    return res.send('It works')
  })
  .use('/user', user)
  ;

module.exports = router;