const { query } = require('../../db');
const { ResError, errorNames } = require('../../utils/error.class');
const { generateSalt, generatePwdSHA512, checkPwd } = require('../../utils/password');
const jwt = require('jsonwebtoken');
const { secret } = require('../../config');

/**
 * @param {import('express').Request} req
 * @param {import('express').Response} res
 * @param {import('express').NextFunction} next
 */
module.exports.create = async (req, res, next) => {
  try {
    if (!req.body.passwordCheck) {
      return next(new ResError({
        message: 'Missing parameter "passwordCheck"',
        name: errorNames.MISSING_PWDCHECK,
      }));
    }
    const username = req.body.username;
    const user = await query('SELECT id FROM users WHERE LOWER(username) = $1;', [username.toLowerCase()])
    if (user.rows.length > 0) {
      return next(new ResError({
        message: 'Username already exists',
        name: errorNames.USERNAME_EXISTS,
      }));
    }
    const pwd = req.body.password;
    if (pwd.length < 8) {
      return next(new ResError({
        message: 'Password too short, must be at least 8 characters long',
        name: errorNames.PWD_TOO_SHORT,
      }))
    }
    const pwdCheck = req.body.passwordCheck;
    if (pwd !== pwdCheck) {
      return next(new ResError({
        message: 'Password and verification doesn\'t match',
        name: errorNames.WRONG_PWDCHECK,
      }));
    }

    const salt = generateSalt(12);
    const pwdHash = generatePwdSHA512(pwd, salt);

    const q = `
      INSERT INTO users(username, password, salt)
      VALUES ($1, $2, $3);
    `;

    const result = await query(q, [username, pwdHash, salt]);
    console.log(result);

    return res.json({
      success: true,
      message: `User ${username} created`,
      more: result,
    });
  } catch (err) {
    return next(err);
  }
}

/**
 * @param {import('express').Request} req
 * @param {import('express').Response} res
 * @param {import('express').NextFunction} next
 */
module.exports.auth = async (req, res, next) => {
  try {
    const usernameBody = req.body.username;
    const user = await query('SELECT id, password, salt, role, username FROM users WHERE LOWER(username) = $1;', [usernameBody.toLowerCase()]);
    if (user.rows.length !== 1) {
      return next(new ResError({
        message: 'Username not found',
        name: errorNames.NO_USER,
      }, 401));
    }
    const password = req.body.password;
    const { password: hash, salt, role, username, id } = user.rows[0];
    if (!checkPwd(hash, password, salt)) {
      return next(new ResError({
        message: 'Password doesn\'t match',
        name: errorNames.WRONG_PWD,
      }, 401));
    }
    const payload = {
      id: parseInt(id),
      username, 
      role,
    };
    const token = jwt.sign(payload, secret, {
      expiresIn: '60d',
    });

    return res.json({
      success: true,
      message: 'Authentication successful',
      token,
    })

  } catch (err) {
    return next(err);
  }

}