const router = require('express').Router({ mergeParams: true });

const { password, username, id } = require('../../middleware/check-param');
const { checkJwt } = require('../../middleware/check-jwt');

const idRoute = require('./id');

const { create, auth } = require('./controller');

router
  .get('', (req, res) => {
    return res.json({
      success: true,
      message: 'User works',
    })
  })

  .post('/create', [username, password, create])
  .post('/auth', [username, password], auth)

  .use('/:idUser', id, checkJwt, idRoute)
  ;

module.exports = router;