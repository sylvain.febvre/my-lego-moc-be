const { query } = require('../../../db');

/**
 * @param {import('express').Request} req
 * @param {import('express').Response} res
 * @param {import('express').NextFunction} next
 */
module.exports.mocs = async (req, res, next) => {
  try {
    const q = `
      SELECT m.id, m.id_user, u.username, m.img_url, m.studio_url, m.name, m.description, m.download, m.vote, m.vue, m.created
      FROM mocs m
      INNER JOIN users u ON u.id = m.id_user
      WHERE u.id = $1
    `;
    const mocs = await query(q, [req.params.idUser]);

    res.json(mocs.rows);
  } catch (err) {
    return next(err);
  }
}