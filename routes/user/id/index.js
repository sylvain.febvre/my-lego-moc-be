const router = require('express').Router({ mergeParams: true });

const { mocs } = require('./controller');

router
  .get('', (req, res) => {
    return res.json({
      success: true,
      message: 'Id works ' + req.params.idUser,
    })
  })
  .get('/mocs', mocs)

  ;

module.exports = router;