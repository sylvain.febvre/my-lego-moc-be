const Pool = require('pg-pool');
const { poolConfig } = require('../config/index');

/** @type {Pool} */
let pool;

module.exports.init = () => {
    pool = new Pool(poolConfig);
}

/**
 * @param {string} query
 * @param {Array} values
 */
module.exports.query = (query, values) => {
    return pool.query(query, values);
}