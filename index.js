const http = require('http');
const express = require('express');
const db = require('./db');

const cors = require('cors');
const { logRequest } = require('./middleware/log-request');
const { errorHandler } = require('./middleware/error-handler');

const { logApp } = require('./logger/logger');

const route = require('./routes/routes');

db.init();

const app = express();
let httpServer = http.createServer(app);

app.use(cors());
app.use(express.json());
app.use(express.urlencoded({ extended: true }));
app.use(logRequest);

app.use('', route);

app.use(errorHandler);

httpServer.listen(3000)
  .on('listening', () => {
    logApp.info(`Web server listening on port 3000`);
  })
  .on('error', console.error);