const { logApp } = require('../logger/logger');
const { ResError } = require('../utils/error.class');

/*eslint no-unused-vars: 0*/
/**
 * @param {Error} err The error object
 * @param {import('express').Request} req Requete express
 * @param {import('express').Response} res Reponse express
 * 
 */
module.exports.errorHandler = (err, req, res, next) => {
  const response = {
    name: err.name,
    message: err.message,
    method: req.method,
    protocol: req.protocol,
    host: req.hostname,
    url: req.url,
    fullUrl: `${req.protocol}://${req.hostname}${req.url}`,
  };
  if (err instanceof ResError) {
    logApp.info(`${response.message}
    at ${response.url}`);
    response.statusCode = err.statusCode;
  } else {
    logApp.error(err.stack);
    response.statusCode = 500;
  }
  return res.status(response.statusCode).json({
    success: false,
    message: response.message,
    error: response,
  });
};