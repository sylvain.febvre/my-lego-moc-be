const { ResError, errorNames } = require('../utils/error.class');

module.exports.username = (req, res, next) => {
  try {
    return req.body.username ? next() : next(new ResError({
      message: 'Missing parameter "username"',
      name: errorNames.MISSING_USERNAME,
    }));
  } catch (err) {
    return next(err);
  }
}

module.exports.password = (req, res, next) => {
  try {
    return req.body.password ? next() : next(new ResError({
      message: 'Missing parameter "password"',
      name: errorNames.MISSING_PWD,
    }));
  } catch (err) {
    return next(err);
  }
}

module.exports.id = (req, res, next) => {
  try {
    const id = req.params.idUser;
    if(isNaN(parseInt(id))) {
      return next(new ResError({
        message: `Param ${id} must be a number`,
        name: errorNames.NOT_A_NUMBER,
      }));
    }
    return next();
  } catch (err) {
    return next(err);
  }
}