const { logReq } = require('../logger/logger');
const onFinished = require('on-finished');

/**
 * @param {import('express').Request} req Requete express
 * @param {import('express').Response} res Reponse express
 * 
 */
module.exports.logRequest = (req, res, next) => {
  onFinished(res, () => {
    const message = `(${process.env.NODE_APP_INSTANCE || 0}) Received request ${req.method} on ${req.originalUrl} from ${req.ip} => Status [${res.statusCode}]`;
    logReq.info(message);
  });
  return next();
};