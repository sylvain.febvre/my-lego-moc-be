const { ResError, errorNames } = require('../utils/error.class');
const jwt = require('jsonwebtoken');
const { secret } = require('../config');

/**
 * @param {import('express').Request} req
 * @param {import('express').Response} res
 * @param {import('express').NextFunction} next
 */
module.exports.checkJwt = async (req, res, next) => {
  const authHeader = req.get('authorization');
  //Header exists
  if (!authHeader) {
    return next(new ResError({
      message: 'No token provided',
      name: errorNames.JWT_NO_TOKEN,
    }, 403));
  }
  const parts = authHeader.split(' ');
  //Correct format ("Bearer {token}")
  if (parts.length !== 2 || parts[0] !== 'Bearer') {
    return next(new ResError({
      message: 'Wrong header format',
      name: errorNames.JWT_WRONG_HEADER,
    }, 403));
  }
  const token = parts[1];
  try {
    const decoded = jwt.verify(token, secret);
    //Not expired
    if (decoded.exp <= Date.now().valueOf() / 1000) {
      return next(new ResError({
        message:'Token expired, please log again',
        name: errorNames.JWT_EXPIRED,
      }));
    }
    //Check if user sending request match id
    if(parseInt(req.params.idUser) !== decoded.id) {
      return next(new ResError({
        message: 'You are trying to access another user',
        name: errorNames.USERID_DIFFER,
      }, 403));
    }
    return next();
  } catch (err) {
    //Fail to verify
    return next(new ResError({
      message: 'Failed to authenticate token',
      name: errorNames.JWT_FAIL_VERIFY,
    }, 403));
  }
}