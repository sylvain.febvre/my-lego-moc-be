module.exports.ResError = class ResError extends Error {
  constructor({ message, name = 'ERROR' }, statusCode = 400) {
    super(message);
    this.name = name;
    this.statusCode = statusCode;
  }
};

module.exports.errorNames = Object.freeze({
  NO_USER: 1,
  WRONG_PWD: 2,
  USERNAME_EXISTS: 3,
  PWD_TOO_SHORT: 4,
  WRONG_PWDCHECK: 5,
  NOT_A_NUMBER: 6,
  USERID_DIFFER: 7,

  MISSING_USERNAME: 100,
  MISSING_PWD: 101,
  MISSING_PWDCHECK: 102,

  JWT_NO_TOKEN: 200,
  JWT_WRONG_HEADER: 201,
  JWT_FAIL_VERIFY: 202,
  JWT_EXPIRED: 203,


})