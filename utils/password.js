const crypto = require('crypto');

module.exports.generateSalt = (length) => {
  return crypto.randomBytes(Math.ceil(length / 2)).toString('hex').slice(0, length);
}

module.exports.generatePwdSHA512 = (pwd, salt) => {
  return crypto.createHmac('sha512', salt).update(pwd).digest('hex');
}

module.exports.checkPwd = (hash, pwd, salt) => {
  return this.generatePwdSHA512(pwd, salt) === hash;
}