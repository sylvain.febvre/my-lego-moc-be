const winston = require("winston");
const { createLogger, format, transports } = winston;
const { combine, timestamp, printf } = format;
require("winston-daily-rotate-file");
const path = require("path");

const logFilePath = process.env.DIRLOG || path.join(__dirname, "..", "logs");

const logFileFormat = combine(
  timestamp({ format: "DD-MM-YYYY HH:mm:ss.SSS" }),
  printf(
    ({ level, message, timestamp }) => `${timestamp} - ${level}: ${message}`
  )
);

/**
 * Création du logger de l'application
 */
const logApp = createLogger({
  transports: [
    new winston.transports.DailyRotateFile({
      format: logFileFormat,
      filename: path.join(
        logFilePath,
        "%DATE%",
        `cluster_${process.env.NODE_APP_INSTANCE || 0}.error.log`
      ),
      level: "error"
    }),
    new winston.transports.DailyRotateFile({
      format: logFileFormat,
      filename: path.join(
        logFilePath,
        "%DATE%",
        `cluster_${process.env.NODE_APP_INSTANCE || 0}.log`
      )
    })
  ]
});

/**
 * Création du logger des requetes
 */
const logReq = createLogger({
  transports: [
    new winston.transports.DailyRotateFile({
      format: logFileFormat,
      filename: path.join(logFilePath, "%DATE%", `request.log`)
    })
  ]
});

if (process.env.NODE_ENV !== "production") {
  logApp.add(
    new transports.Console({
      format: format.simple()
    })
  );
  logReq.add(
    new transports.Console({
      format: format.simple()
    })
  );
}

module.exports.logApp = logApp;
module.exports.logReq = logReq;
module.exports.logPath = logFilePath;
